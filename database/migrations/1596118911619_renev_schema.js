'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class RenevSchema extends Schema {
  up () {
    this.create('renevs', (table) => {
      table.increments()
      table.string('no_rab')
      table.string('tanggal')
      table.string('anggaran')
      table.string('nama_pekerjaan')
      table.string('nilai')
      table.string('scan_rab')
      table.string('status_berkas')
      table.string('tgl_process')
      table.string('tgl_closed')
      table.timestamps()
    })
  }

  down () {
    this.drop('renevs')
  }
}

module.exports = RenevSchema

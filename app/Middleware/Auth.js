'use strict'
/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

class Auth {
  /**
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Function} next
   */
  async handle ({ request, auth }, next) {
    // try {
    //   await auth.check()
    // } catch (error) {
    //   return response.unauthorized({
    //     status: 401,
    //     error: true,
    //     data: null,
    //     message: 'Unauthorize Access'
    //   })
    // }
    const cek = await auth.check()
    console.log(cek)
    // call next to advance the request
    await next()
  }
}

module.exports = Auth

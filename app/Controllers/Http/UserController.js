'use strict'

const User = use('App/Models/User')

class UserController {
  async index({response}) {
    const user = await User.all()

    return response.ok({
      status: 200,
      error: false,
      data: user,
      message: null
    })
  }
  async store({request, response}) {
    const user = request.only(['username', 'email' , 'password',])
    const storeUser = await User.create(user)

    return response.ok({
      status: 200,
      error: false,
      data: storeUser,
      message: 'User Berhasil Ditambahkan !'
    })
  }
}

module.exports = UserController

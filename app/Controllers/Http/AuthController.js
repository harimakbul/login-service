'use strict'

const User = use('App/Models/User')

class AuthController {
  async Login({request, response, auth}) {
    const {email, password} = request.post()

    const user = await User.findBy('email', email)
    if (user === null) {
      return response.unauthorized({
        status: 401,
        error: true,
        data: null,
        message: 'Invalid email or password'
      })
    }

    // await user.load('level')
    const token = await auth.attempt(email, password, user.toJSON())
    return response.ok({
      status: 200,
      error: false,
      data: {
        token : token.token,
        user : {
          id : user.id,
          username : user.username,
          email : user.email
        }
      },
      message: 'Login successfully'
    })
  }
  async logout({request, response, auth}) {
    // const logout = await auth.logout()
    const token = request.input('token')
    await auth.authenticator('jwt').revokeTokens([token])
    return response.ok({
      status: 200,
      error: false,
      data: null,
      message: 'Logged Out Successfully'
    })
  }
}

module.exports = AuthController

'use strict'

class Login {
  get rules () {
    return {
      email: 'required|email',
      password: 'required'
    }
  }

  get messages () {
    return {
      'email.required': 'You must provide a email',
      'email.email': 'You must provide a valid email address.',
      'password.required': 'You must provide a password'
    }
  }

  get validateAll () {
    return true
  }

  async fails (errorMessages) {
    const {  response } = this.ctx
    return response.badRequest({
      status: 400,
      error: true,
      data: errorMessages,
      message: 'Validation Error'
    })
  }
}

module.exports = Login
'use strict'
/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

class CekAuth {
  /**
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Function} next
   */
  async handle ({ request, auth, response }, next) {
    // call next to advance the request
    try {
      await auth.check()
    } catch (error) {
      return response.unauthorized({
        status: 401,
        error: true,
        data: null,
        message: 'Unauthorize Access'
      })
    }
    await next()
  }
}

module.exports = CekAuth
